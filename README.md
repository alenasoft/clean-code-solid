# Curso Clean Code y Principios SOLID #

[Diapositivas del curso](https://docs.google.com/presentation/d/1xbqthxHmyUjDKzRISeFBLQAPepYkLi4uMW04FXW_3LM/edit?usp=sharing)

El código fuente del proyecto esta basado en un proyecto Java [Maven](https://maven.apache.org/).

## Software a instalar ##

 1. [Java Development Kit 8 (JKD)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
 2. [IntelliJ IDEA](https://www.jetbrains.com/idea/). Community Version

## Instrucciones para abrir el proyecto ##

1. Ejecutar IntelliJ
2. En la pantalla inicial seleccionar la opción ***Check out from Version Control*** -> ***Git***
3. En URL pegar la dirección *https://bitbucket.org/alenasoft/clean-code-solid.git*
4. Establecer un folder donde se guardará el proyecto
5. Asignar un nombre el proyecto
6. Presionar el botón **Clone** y aceptar la creación del proyecto.
7. Aceptar la opción de abrir el archivo **pom.xml**
  Sugerencia:
 - Habilitar auto importación (Enable Auto-Import)

## Bibliografía ##
- Martin, Robert C. (2008). *Clean Code: A Handbook of Agile Software Craftsmanship*. Prentice Hall.
