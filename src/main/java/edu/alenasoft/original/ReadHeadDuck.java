package edu.alenasoft.original;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class ReadHeadDuck extends Duck {
    public void display() {
        System.out.println("I'm a RedHead duck");
    }
}
