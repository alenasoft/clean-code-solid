package edu.alenasoft.original;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public abstract class Duck {

    public void quack() {
        System.out.println("QUACK QUACK!!!");
    }
    public void swim() {
        System.out.println("I am swimming");
    }

    public void fly() {
        System.out.println("I can fly");
    }

    public abstract void display();
}
