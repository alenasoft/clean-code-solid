package edu.alenasoft.original;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class RubberDuck extends Duck {

    @Override
    public void fly() {
        // Disable fly
    }

    @Override
    public void quack() {
        System.out.println("SQUEAK SQUEAK ...");
    }

    @Override
    public void display() {
        System.out.println("I am a Rubber Duck");
    }
}
