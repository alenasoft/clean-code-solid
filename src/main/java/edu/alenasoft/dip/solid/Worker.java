package edu.alenasoft.dip.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public interface Worker {

    void doSomeWork();
}
