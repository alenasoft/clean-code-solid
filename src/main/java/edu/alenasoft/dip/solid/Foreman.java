package edu.alenasoft.dip.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Foreman {

    private Worker worker;

    public void assignWorker(Worker worker) {
        this.worker = worker;
    }

    public void manageWork() {
        worker.doSomeWork();
    }
}
