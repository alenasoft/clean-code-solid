package edu.alenasoft.dip.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Main {

    public static void main(String[] args) {
        Worker painter = new Painter();
        Worker roofer = new Roofer();

        Foreman foremanSky = new Foreman();
        foremanSky.assignWorker(roofer);

        Foreman foremanWalls = new Foreman();
        foremanWalls.assignWorker(roofer);

        Foreman foremanClasses = new Foreman();

        foremanClasses.assignWorker(new Worker() {
            public void doSomeWork() {
                // teach
            }
        });
    }
}
