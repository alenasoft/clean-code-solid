package edu.alenasoft.dip.original;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Foreman {

    private Roofer roofer;

    public void assignWorker(Roofer roofer) {
        this.roofer = roofer;
    }

    public void manageWork() {
        roofer.doSomeWork();
    }
}
