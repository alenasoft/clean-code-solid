package edu.alenasoft.isp.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public interface WriteableStream {

    void write();
}
