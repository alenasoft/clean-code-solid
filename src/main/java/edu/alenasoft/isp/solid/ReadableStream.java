package edu.alenasoft.isp.solid;

import java.nio.Buffer;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public interface ReadableStream {

    Buffer read();
}
