package edu.alenasoft.isp.original;

import java.nio.Buffer;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public interface Stream {

    void open();
    Buffer read();
    void write(Buffer buffer);
}
