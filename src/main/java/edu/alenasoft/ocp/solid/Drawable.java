package edu.alenasoft.ocp.solid;

import java.awt.*;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public interface Drawable {

    void draw(Graphics g);
}
