package edu.alenasoft.ocp.solid;

import java.awt.*;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 100);
        AreaCalculator calculator = new AreaCalculator();

        // 1000
        calculator.calculateArea(rectangle);

        Circle circle = new Circle(15);
        calculator.calculateArea(circle);

        Triangle triangle = new Triangle(80,60);

        calculator.calculateArea(triangle);

        Shape square = new Rectangle(10,10);
        calculator.calculateArea(square);
        Rectangle cloneSquare = (Rectangle) square;

        calculator.calculateArea(cloneSquare);

    }
}
