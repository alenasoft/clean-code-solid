package edu.alenasoft.ocp.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public interface Shape {

    double getArea();
}
