package edu.alenasoft.ocp.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class AreaCalculator {

    public double calculateArea(Shape shape) {
        return shape.getArea();
    }
}
