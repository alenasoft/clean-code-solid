package edu.alenasoft.ocp.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Triangle implements Shape {

    private int height;
    private int width;

    public Triangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public double getArea() {
        return 0.5*height*width;
    }
}
