package edu.alenasoft.ocp.solid;

import java.awt.*;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Circle implements Shape, Drawable {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public void draw(Graphics g) {
        //dibujar
    }
}
