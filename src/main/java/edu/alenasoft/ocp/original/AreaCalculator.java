package edu.alenasoft.ocp.original;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class AreaCalculator {

    public int calculateRectangleArea(Rectangle rectangle) {
        return rectangle.getHeight() * rectangle.getWidth();
    }

    public double calculateCircleArea(Circle circle) {
        return Math.PI * circle.getRadius() * circle.getRadius();
    }
}
