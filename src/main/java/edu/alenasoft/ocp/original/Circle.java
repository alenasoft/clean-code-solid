package edu.alenasoft.ocp.original;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Circle {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }
}
