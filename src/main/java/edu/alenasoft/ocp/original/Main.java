package edu.alenasoft.ocp.original;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 100);
        AreaCalculator calculator = new AreaCalculator();

        // 1000
        calculator.calculateRectangleArea(rectangle);

        Circle circle = new Circle(15);
        calculator.calculateCircleArea(circle);
    }
}
