package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class RubberDuck extends Duck {

    public RubberDuck() {
        this.setFlyBehavior(new FlyNoWay());
        this.setSoundBehavior(new SqueckBehavior());
    }

    @Override
    public void quack() {
        System.out.println("SQUEAK SQUEAK ...");
    }

    @Override
    public void display() {
        System.out.println("I am a Rubber Duck");
    }
}
