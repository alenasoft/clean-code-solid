package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class ReadHeadDuck extends Duck {

    public ReadHeadDuck() {
        this.setFlyBehavior(new FlyWithWings());
        this.setSoundBehavior(new QuackBehavior());
    }

    public void display() {
        System.out.println("I'm a RedHead duck");
    }
}
