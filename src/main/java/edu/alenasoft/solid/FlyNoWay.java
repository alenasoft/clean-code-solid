package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class FlyNoWay implements FlyBehavior {
    public void fly() {}
}
