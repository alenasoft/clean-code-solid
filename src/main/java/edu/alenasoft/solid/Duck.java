package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class Duck {

    private FlyBehavior flyBehavior;
    private SoundBehavior soundBehavior;

    public void quack() {
        this.soundBehavior.play();
    }
    public void swim() {
        System.out.println("I am swimming");
    }

    public void display() { }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setSoundBehavior(SoundBehavior soundBehavior){
        this.soundBehavior = soundBehavior;
    }

    public void fly() {
        this.flyBehavior.fly();
    }
}
