package edu.alenasoft.solid;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class Game {
    public static void main(String[] args) {
        List<Duck> ducks = new ArrayList<Duck>();
        ducks.add(new MallardDuck());
        ducks.add(new ReadHeadDuck());
        ducks.add(new RubberDuck());
        ducks.add(new RocketDuck());

        /*
        Duck customDuck = new Duck();
        customDuck.setFlyBehavior(new FlyTinkeBell());
        customDuck.setQuackBehavior(new sayHastaLaVista());
        customDuck.setDisplayBehavior(new zombieDisplay());
        ducks.add(customDuck);
        */


        for (Duck duck : ducks) {
            duck.quack();
            duck.fly();
        }
    }
}
