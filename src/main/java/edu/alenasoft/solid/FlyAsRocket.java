package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class FlyAsRocket implements FlyBehavior {
    public void fly() {
        System.out.println("Fly as a Rocket");
    }
}
