package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class MallardDuck extends Duck {

    public MallardDuck() {
        this.setFlyBehavior(new FlyWithWings());
        this.setSoundBehavior(new QuackBehavior());
    }

    public void display() {
        System.out.println("I'm mallard");
    }
}
