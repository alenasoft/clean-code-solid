package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class RocketDuck extends Duck {

    public RocketDuck() {
        this.setFlyBehavior(new FlyAsRocket());
        this.setSoundBehavior(new QuackBehavior());
    }
}
