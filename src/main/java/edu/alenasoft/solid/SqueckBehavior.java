package edu.alenasoft.solid;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class SqueckBehavior implements SoundBehavior {

    public void play() {
        System.out.println("Squeck Squeck.. ");
    }
}
