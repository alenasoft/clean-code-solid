package edu.alenasoft.optiona;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public abstract class Duck {

    public void quack() {
        System.out.println("QUACK QUACK!!!");
    }
    public void swim() {
        System.out.println("I am swimming");
    }

    public abstract void display();
}
