package edu.alenasoft.optiona;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public interface Flyable {

    void fly();
}
