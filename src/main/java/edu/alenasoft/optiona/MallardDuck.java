package edu.alenasoft.optiona;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class MallardDuck extends Duck implements Flyable {
    public void display() {
        System.out.println("I'm mallard");
    }

    public void fly() {
        System.out.println("Flying");
    }
}
