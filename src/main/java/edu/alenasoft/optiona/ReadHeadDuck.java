package edu.alenasoft.optiona;

/**
 * Created by Luis Roberto Perez on 22/10/2016.
 */
public class ReadHeadDuck extends Duck implements Flyable {
    public void display() {
        System.out.println("I'm a RedHead duck");
    }

    public void fly() {
        System.out.println("Flying");
    }
}
