package edu.alenasoft.lsp.original;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class Rectangle extends Shape {
    private int height;
    private int width;

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
