package edu.alenasoft.lsp.original;

import java.awt.*;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class ShapeDrawer {

    public void drawShape(Shape shape) {
        if(shape instanceof Rectangle) {
            drawRectangle( (Rectangle) shape);
        } else {
            if (shape instanceof Circle) {
                drawCirle( (Circle) shape );
            }
        }
    }

    private void drawRectangle(Rectangle rectangle) {
        // Como dibujar el rectangulo
    }

    private void drawCirle(Circle circle) {
        // Como dibujar el circulo
    }
}
