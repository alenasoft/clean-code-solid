package edu.alenasoft.lsp.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public abstract class Shape {

    private String name;

    public String showName() {
        return this.name;
    }

    abstract void draw();
}
