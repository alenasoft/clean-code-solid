package edu.alenasoft.lsp.solid;

/**
 * Created by Luis Roberto Perez on 29/10/2016.
 */
public class ShapeDrawer {

    public void drawShape(Shape shape) {
        shape.draw();
    }
}
